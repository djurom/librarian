<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

use App\Form\BookType;
use App\Entity\Book;
use App\Entity\Author;

class BookAndAuthorsController extends AbstractController
{
    #[Route('/book-and-authors', name: 'book_and_authors')]
    public function newBookAndAuthors(Request $request, EntityManagerInterface $entityManager): Response
    {
        $book = new Book();

        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirectToRoute('book_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book_and_authors/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/book-and-authors/{id}/edit', name: 'book_and_authors_edit', methods: ['GET','POST'])]
    public function bookAndAuthorsEdit(Request $request, Book $book, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirectToRoute('book_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book_and_authors/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
