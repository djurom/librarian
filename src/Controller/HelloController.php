<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{
    #[Route('/', name: 'default_route')]
    #[Route('/hello', name: 'hello')]
    public function index(): Response
    {
        $message = "Symfony Developer!";
        return $this->render('hello/index.html.twig', [
            'message' => $message
        ]);
    }
}
